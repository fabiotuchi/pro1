package utilities;

import java.awt.*;
import java.awt.event.*;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.swing.*;
import javax.swing.Timer;
 
public class TimerTime extends JPanel implements ActionListener, Runnable
{
	Calendar cal = Calendar.getInstance();
	final String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm:ss";		
	SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
    private JLabel timeLabel = new JLabel( sdf.format(new Date()));

    public final JLabel getTimeLabel() {
        return timeLabel;
	}
	

    public TimerTime()
    {        
    	add( timeLabel );
        Timer timer = new Timer(1000, this);
        timer.setInitialDelay(1);
        timer.start();
    }
 
    @Override
    public void actionPerformed(ActionEvent e)
    {
        timeLabel.setText( sdf.format(new Date()) );
    }
 
    private static void createAndShowUI()
    {
        JFrame frame = new JFrame("TimerTime");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add( new TimerTime() );
        frame.setLocationByPlatform( true );
        frame.pack();
        frame.setVisible( true );
    }

	@Override
	public void run() {
		EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                createAndShowUI();
            }
        });	
	}
 
    /*
    public static void main(String[] args)
    {
        EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                createAndShowUI();
            }
        });
    }
    */
}