package utilities;


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellUtil;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.sikuli.script.FindFailed;

import sikulix.ImageFrame;

public class GuardarCaptura {
    static String EXCEL_DE_ENTRADA="resources/data.xlsx";
    static InputStream archivoExcelDeEntrada = null;
    static XSSFWorkbook  libroEntrada = null;
    static XSSFSheet hojaEntrada = null;
    static XSSFRow filaEntrada=null; 
    static XSSFCell celdaEntrada=null;
    //static byte[] imageBytesAE = null;
    //static int pictureureAEIdx = 0;

    static int cantFilas = 0;
    static int cantColumnas = 0;
    
    static int fila=0;
    static int columna=0;
    
    static String EXCEL_DE_SALIDA = null;
    static FileOutputStream archivoExcelDeSalida = null;
    static Workbook libroSalida = null;
    static Sheet hojaSalida = null;
    static Row filaSalida=null; 
    static Cell celdaSalida=null;
    
    /*
    static byte[] imageBytesAS = null;
    static int pictureureASIdx = 0;
    
    static CreationHelper helper = null;
    static Drawing drawing = null;
    static ClientAnchor ancla = null;
    static Picture  dibujoImagen=null;
    static InputStream archivoImagen=null;
*/
    
    static FileOutputStream fileOut = null;
    static XSSFWorkbook wb=null;
        
    public static void leerExcel() throws IOException
    {
        archivoExcelDeEntrada = new FileInputStream(EXCEL_DE_ENTRADA);    
        libroEntrada = new XSSFWorkbook(archivoExcelDeEntrada);
        hojaEntrada = libroEntrada.getSheetAt(0);
        
        Iterator rows = hojaEntrada.rowIterator();
        
        rows.next(); //Paso a la segunda fila ignorando los t�tulos de las columnas..
        
        while (rows.hasNext())
        {
            cantColumnas=0;
            //rows.next();
            
            filaEntrada=(XSSFRow) rows.next();
            Iterator cells = filaEntrada.cellIterator();
            
            while (cells.hasNext())
            {                
                celdaEntrada=(XSSFCell) cells.next();
                
                //System.out.println(celdaEntrada.getStringCellValue());
                
                switch (celdaEntrada.getCellType()) {
                    case XSSFCell.CELL_TYPE_NUMERIC:
                        System.out.print(celdaEntrada.getNumericCellValue() + "(Integer)\t");
                        break;
                    case XSSFCell.CELL_TYPE_STRING:
                        System.out.print(celdaEntrada.getStringCellValue() + "(String)\t");
                        break;
                    case XSSFCell.CELL_TYPE_BLANK:
                        System.out.print(celdaEntrada.getStringCellValue() + "(Blank)\t");
                        break;
                    case XSSFCell.CELL_TYPE_BOOLEAN:
                        System.out.print(celdaEntrada.getCellFormula() + "(Boolean)\t");
                        break;
                }
                
                
                cantColumnas++;
            }
            System.out.println();
            cantFilas++;
        }//del while
        
        System.out.println("Cant. filas= "+cantFilas+" - Cant. columnas="+cantColumnas);
    
    }
    
    public void SacarFoto(String nombreimagen, String nombreimagen2, String nombre) throws FindFailed, IOException {
		EditorDeImagen EDI = new EditorDeImagen();
		String outputImagePath = "C:\\Users\\ljerez\\Desktop\\numeros\\" + nombre + ".png";
		EDI.editimage2(nombreimagen, nombreimagen2, nombre);
		ImageFrame f = new ImageFrame(outputImagePath);
	}
    
    public static void guardarImagen(String cadena, String nombreImagen) throws IOException {
          
        try {
               wb = new XSSFWorkbook();
                Sheet sheet = wb.createSheet("Hoja1");;
                String nI=("./logs/").concat(nombreImagen);
                //wb.setActiveSheet(0);
               
// ----------------------- DEFINO ALGUNOS FORMATOS PARA UTILIZAR -------------------
                XSSFCellStyle style = wb.createCellStyle();
               style.setVerticalAlignment(VerticalAlignment.TOP);
               style.setAlignment(HorizontalAlignment.CENTER);
               
               
                Map<String, Object> properties = new HashMap<String, Object>();
                // border around a cell
                properties.put(CellUtil.BORDER_TOP, BorderStyle.THIN);
                properties.put(CellUtil.BORDER_BOTTOM, BorderStyle.THIN);
                properties.put(CellUtil.BORDER_LEFT, BorderStyle.THIN);
                properties.put(CellUtil.BORDER_RIGHT, BorderStyle.THIN);
               
               
                Row row= sheet.createRow(fila);
               Cell celda = null;
               
               //Defino ancho de columna
                int widthUnits = 20000; 
                sheet.setColumnWidth(columna, widthUnits);
                sheet.setColumnWidth(columna+1, widthUnits);    
                sheet.setColumnWidth(columna+2, widthUnits);
// ----------------------- FIN DE DEFINICION DE FORMATOS -------------------

                
// -------------------------------ESCRIBO LOS TITULOS DE LAS COLUMNAS -------------------------------------                
                //Creo la celda A1
               celda = row.createCell(columna);
               celda.setCellStyle(style);
               
                String updateCell = celda.getStringCellValue();
                
               if(updateCell == null || updateCell.isEmpty()) {
                    
                    //Me posiciono en la celda A1
                    sheet.showInPane(fila, columna); 
                    CellUtil.setCellStyleProperties(celda, properties);
         
                    //Escribo el t�tulo de la columna A
                    celda.setCellValue("Informaci�n");
                    CellUtil.setCellStyleProperties(celda, properties);
                    
               } else {
                   System.out.println("Se intent� escribir en una celda que ten�a datos!!");
               }
                               
                //Creo la celda B1
                celda = row.createCell(columna+1);
                
                //Escribo el t�tulo de la columna B
               celda.setCellValue("Captura de pantalla");
               
               //Aplico estilo a la celda
               celda.setCellStyle(style);
               CellUtil.setCellStyleProperties(celda, properties);

                //Me posiciono en la celda B1
               //sheet.showInPane(fila, columna+1);        
                //celda.setAsActiveCell();

               
// -------------------------------FIN DE LOS TITULOS DE LAS COLUMNAS -------------------------------------

// -------------------------------INSERTO REGISTROS DE DATOS ---------------------------------------------

               /*
                //Creo la celda A2
                cell = sheet.createRow(fila+1).createCell(columna);
               
                //Me posiciono en la celda A2
               sheet.showInPane(fila+1, columna);
                CellUtil.setCellStyleProperties(cell, properties);
                cell.setAsActiveCell();
                
               //Escribo el la informaci�n en la columna A
                cell.setCellValue(new SimpleDateFormat("ddMMMMyyyy_HHmmss'.xlsx'").format(new Date())+
                        " - "+cadena);
                 */
               
             //Defino alto de fila
                //short heightUnits = 1000; 
                //sheet.setDefaultRowHeight(heightUnits);
               
               /*
               
         // ---------------------- B2  (Fila 2, Columna 2) ---------------
                //Creo la celda B2
                cell = sheet.createRow(fila+1).createCell(columna+1);
               
                //Me posiciono en la celda B2
                sheet.showInPane(fila+1, columna+1);                        
                CellUtil.setCellStyleProperties(cell, properties);
                cell.setAsActiveCell();

                //FileInputStream obtains input bytes from the image file
                InputStream inputStream = new FileInputStream(nI);
                
                //Get the contents of an InputStream as a byte[].
                byte[] bytes = IOUtils.toByteArray(inputStream);
                //Adds a picture to the workbook
                int pictureIdx = wb.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
                //close the input stream
                inputStream.close();
                //Returns an object that handles instantiating concrete classes
                CreationHelper helper = wb.getCreationHelper();
                //Creates the top-level drawing patriarch.
                Drawing drawing = sheet.createDrawingPatriarch();
                //Create an anchor that is attached to the worksheet
                ClientAnchor anchor = helper.createClientAnchor();
                //create an anchor with upper left cell _and_ bottom right cell
                anchor.setCol1(1); //Column B 
                //sheet.autoSizeColumn(0);
                anchor.setRow1(1); //Row 3
                anchor.setCol2(2); //Column C
                anchor.setRow2(2); //Row 4           
                //Creates a picture
                Picture pict = drawing.createPicture(anchor, pictureIdx); //pict.resize(); //don't do that. Let the anchor resize the image!
                */
// ------------------------------- FIN DE INSERCION DE REGISTROS DE DATOS -----------------------------------------

                Date fecha = new Date();
                Timestamp hora= new Timestamp(fecha.getTime());
                String hh=hora.toString();
                  
                EXCEL_DE_SALIDA = "./logs/registro"+new SimpleDateFormat("ddMMMMyyyy_HHmmss'.xlsx'").format(new Date());
                //System.out.println("Nombre del Reporte: "+EXCEL_DE_SALIDA);
                
                fileOut = new FileOutputStream(EXCEL_DE_SALIDA);
                 wb.write(fileOut);

               } catch (Exception ioex) {
               }
        
    } // fin guardarImagen

    public static void escribirExcel() throws IOException {                
        /*
        archivoExcelDeSalida = new FileOutputStream(new File(EXCEL_DE_SALIDA));
        libroSalida = new XSSFWorkbook();
        hojaSalida = libroSalida.createSheet("Hoja1");
        */
        
        try {
              XSSFWorkbook wb = new XSSFWorkbook();
               Sheet sheet = wb.createSheet("Hoja1");
               //FileInputStream obtains input bytes from the image file
               InputStream inputStream = new FileInputStream("./logs/Login.jpg");
               //Get the contents of an InputStream as a byte[].
               byte[] bytes = IOUtils.toByteArray(inputStream);
               //Adds a picture to the workbook
               int pictureIdx = wb.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
               //close the input stream
               inputStream.close();
               //Returns an object that handles instantiating concrete classes
               CreationHelper helper = wb.getCreationHelper();
               //Creates the top-level drawing patriarch.
               Drawing drawing = sheet.createDrawingPatriarch();
               //Create an anchor that is attached to the worksheet
               ClientAnchor anchor = helper.createClientAnchor();
               //create an anchor with upper left cell _and_ bottom right cell
               anchor.setCol1(1); //Column B 
               //sheet.autoSizeColumn(0);
               anchor.setRow1(1); //Row 3
               anchor.setCol2(2); //Column C
               anchor.setRow2(2); //Row 4           
               //Creates a picture
               Picture pict = drawing.createPicture(anchor, pictureIdx); //pict.resize(); //don't do that. Let the anchor resize the image!
               //Create the Cell B3
               Cell cell = sheet.createRow(1).createCell(1);
               //set width to n character widths = count characters * 256
               int widthUnits = 100*256;
               sheet.setColumnWidth(0, widthUnits);
               sheet.setColumnWidth(1, widthUnits);
               //set height to n points in twips = n * 20
               short heightUnits = 100*60;
               cell.getRow().setHeight(heightUnits);
               
               Date fecha = new Date();
               Timestamp hora= new Timestamp(fecha.getTime());
               String hh=hora.toString();
               FileOutputStream fileOut = null;
               
               EXCEL_DE_SALIDA = "./logs/registro_"+new SimpleDateFormat("ddMMMMyyyy_HHmmss'.xlsx'").format(new Date());
               
               //System.out.println("Nombre del Reporte: "+EXCEL_DE_SALIDA);
               
               fileOut = new FileOutputStream(EXCEL_DE_SALIDA);
                 wb.write(fileOut);
                 
               fileOut.flush();
               fileOut.close();

              } catch (Exception ioex) {
              }
        
        //hojaSalida.autoSizeColumn(0); //ajusta el ancho de la columna de acuerdo al contenido
      /*  
        //itera por las filas
        for (fila=0;fila < cantFilas; fila++ )
        {
            filaSalida = hojaSalida.createRow(fila);

            //iterating c number of columns
            for (columna=0;columna < cantColumnas; columna++ )
            {
                
                celdaSalida = filaSalida.createCell(columna);
                
                archivoImagen = new FileInputStream("./log/Login.jpg");
                imageBytesAS = IOUtils.toByteArray(archivoImagen);
                pictureureASIdx = libroSalida.addPicture(imageBytesAS, Workbook.PICTURE_TYPE_JPEG);
                archivoImagen.close();
                helper = libroSalida.getCreationHelper();
                drawing = hojaSalida.createDrawingPatriarch();
                ancla = helper.createClientAnchor();
                //Punta superior izquierda de la imagen a insertar
                ancla.setCol1(columna);
                ancla.setRow1(fila);
                drawing.createPicture(ancla, pictureureASIdx);
                
                //guardarImagen("./log/Login.jpg", fila, columna);
                
            }
            //guardarImagen("./log/Credenciales.jpg", fila, columna);
        }
*/
        
        /*
        //write this workbook to an Outputstream.
         try {
         
            libroSalida.write(archivoExcelDeSalida);
        } catch (Exception e) {
            System.out.println("No se pudo escribir el excel");
        }
        try {
            archivoExcelDeSalida.flush();
            archivoExcelDeSalida.close();
        } catch (Exception e) {
            System.out.println("No se pudo cerrar el archivo");
        }
        */
    }
    
    public static void main(String[] args) throws IOException {
        
        leerExcel();
        //escribirExcel();
        
        guardarImagen("Pantalla de Login","Login.jpg");
        
        fileOut.flush();
        fileOut.close();
    }

}

