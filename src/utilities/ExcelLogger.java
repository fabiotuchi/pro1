package utilities;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JOptionPane;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellUtil;
import org.apache.commons.logging.impl.Log4JLogger;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFPatriarch;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFCreationHelper;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFPicture;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.sikuli.script.Screen;
import org.sikuli.script.ScreenImage;

import sikulix.ImageFrame;

public class ExcelLogger {
	static Logger logger = Logger.getLogger("Inicio");

	public static void guardarImagen(String cadena, String nombreImagen) throws IOException {

		FileOutputStream archivoExcelDeSalida = null;
		XSSFWorkbook libroSalida = null;
		Sheet hojaSalida = null;
		Cell celda = null;
		XSSFRow filaSalida = null;
		File archivo = null;
		int columna = 0;

		// byte[] imageBytesAS = null;
		// int pictureureASIdx = 0;
		// CreationHelper helper = null;
		// Drawing drawing = null;
		// ClientAnchor ancla = null;
		// Picture dibujoImagen = null;
		Map<String, Object> formatearContenido = null;

		libroSalida = new XSSFWorkbook();
		hojaSalida = libroSalida.createSheet("Hoja1");

//------------------ DEFINO ALGUNOS FORMATOS PARA UTILIZAR ----------------
		XSSFCellStyle alinearContenido = libroSalida.createCellStyle();
		alinearContenido.setVerticalAlignment(VerticalAlignment.TOP);
		alinearContenido.setAlignment(HorizontalAlignment.CENTER);

		formatearContenido = new HashMap<String, Object>();
		// border around a cell
		formatearContenido.put(CellUtil.BORDER_TOP, BorderStyle.THIN);
		formatearContenido.put(CellUtil.BORDER_BOTTOM, BorderStyle.THIN);
		formatearContenido.put(CellUtil.BORDER_LEFT, BorderStyle.THIN);
		formatearContenido.put(CellUtil.BORDER_RIGHT, BorderStyle.THIN);
		formatearContenido.put(CellUtil.WRAP_TEXT, true);

		// Defino ancho de columna
		hojaSalida.setColumnWidth(columna, 6000);
		hojaSalida.setColumnWidth(columna + 1, 20000);
		hojaSalida.setColumnWidth(columna + 2, 25000);

// ----------------------- FIN DE DEFINICION DE FORMATOS -------------------

// -----------------VERIFICO SI EL ARCHIVO EXISTE -----------------------------------
		archivo = new File("./logs/logs.xlsx");
		String target = "./logs/backups/data_" + new SimpleDateFormat("ddMMMMyyyy_HHmmss'.xlsx'").format(new Date());
		Path targetFilePath = Paths.get(target);
		Path sourceFilePath = Paths.get("./logs/logs.xlsx");

		if (archivo.exists() && !archivo.isDirectory()) { // Si el archivo SI existe entra por ac�...

//----------- El archivo existe. Hago el backup.. ----------------------------------------
			try {
				File theDir = new File("./logs/backups/"); // Defining Directory/Folder Name
				try {
					if (!theDir.exists()) { // Checks that Directory/Folder Doesn't Exists!
						boolean result = theDir.mkdir();
						if (result) {
							JOptionPane.showMessageDialog(null, "Se gener� backup!");
						}
					}
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, e);
				}

				// Files.move(sourceFilePath, targetFilePath);
				Files.copy(sourceFilePath, targetFilePath);

			} catch (FileNotFoundException e) {
				// e.printStackTrace();
				System.out.println("Archivo inexistente.No se pudo hacer el backup. Continuamos...");
			}
//--------------------- Fin del backup.. ------------------------------------------

		} else if (!archivo.exists()) {
//------------------- El archivo NO existe ----------------------------------------

// ------------------- ESCRIBO LOS TITULOS DE LAS COLUMNAS -----------------
			// Creo la fila y la enumero Fila 1
			filaSalida = (XSSFRow) hojaSalida.createRow(0);
			filaSalida.setRowNum(0);

			// Creo la celda A1
			celda = filaSalida.createCell(0);
			celda.setCellStyle(alinearContenido);
			CellUtil.setCellStyleProperties(celda, formatearContenido);
			String updateCell = celda.getStringCellValue();
			if (updateCell == null || updateCell.isEmpty()) {
				// Me posiciono en la celda A1
				// hojaSalida.showInPane(fila, columna);
				// Escribo el t�tulo de la columna A
				celda.setCellValue("Fecha y Hora");
			} else {
				System.out.println("Se intent� escribir en una celda que ten�a datos!!");
			}

			// Creo la celda B1
			celda = filaSalida.createCell(1);
			celda.setCellStyle(alinearContenido);
			CellUtil.setCellStyleProperties(celda, formatearContenido);
			// Escribo el t�tulo de la columna B
			celda.setCellValue("Informaci�n");

			// Creo la celda C1
			celda = filaSalida.createCell(2);
			celda.setCellStyle(alinearContenido);
			CellUtil.setCellStyleProperties(celda, formatearContenido);
			// Escribo el t�tulo de la columna C
			celda.setCellValue("Captura de pantalla");

			archivoExcelDeSalida = new FileOutputStream("./logs/logs.xlsx");
			libroSalida.write(archivoExcelDeSalida);
			libroSalida.close();
			archivoExcelDeSalida.flush();
			archivoExcelDeSalida.close();

		} // fin del si el archivo existe

// -------------------- FIN DE LOS TITULOS DE LAS COLUMNAS ---------------------

		// Aqu� el archivo ya existe.

		// try {

		// FileInputStream archivoTemp = new FileInputStream(new File(EXCEL_DE_SALIDA));
		// Workbook libro = WorkbookFactory.create(inputStream);
		// wb = (XSSFWorkbook) WorkbookFactory.create(archivoTemp);

		// hojaSalida = wb.getSheetAt(0);
		// String nI=("./logs/").concat(nombreImagen);

// --------------------- INSERTO REGISTROS DE DATOS----------------------------------
		// get the last row number to append new
		// int rowCount = hojaSalida.getLastRowNum();

		File myFile = new File("./logs/logs.xlsx");
		FileInputStream fis = new FileInputStream(myFile);
		libroSalida = new XSSFWorkbook(fis);
		hojaSalida = libroSalida.getSheet("Hoja1");

		// Creo la fila y la enumero Fila 2
		filaSalida = (XSSFRow) hojaSalida.createRow(1);
		filaSalida.setRowNum(1);

		// Creo la celda A2
		celda = filaSalida.createCell(0);
		celda.setCellStyle(alinearContenido);
		CellUtil.setCellStyleProperties(celda, formatearContenido);
		// Date fecha = new Date();
		// Timestamp hora= new Timestamp(fecha.getTime());
		// String hh=hora.toString();
		String fechaHora = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date());
		celda.setCellValue(fechaHora);

		// Creo la celda B2
		celda = filaSalida.createCell(1);
		celda.setCellStyle(alinearContenido);
		CellUtil.setCellStyleProperties(celda, formatearContenido);
		celda.setCellValue(cadena);

		// Creo la celda C2
		celda = filaSalida.createCell(2);
		celda.setCellStyle(alinearContenido);
		CellUtil.setCellStyleProperties(celda, formatearContenido);

// ===============================================================
		Screen screen = new Screen();
		ScreenImage file = screen.capture(screen.getBounds());

		String outputImagePath = ".//logs//" + nombreImagen + ".png";
		String nI = (".//logs//").concat(nombreImagen);

		ImageFrame f = new ImageFrame(outputImagePath);

		System.out.println("Saved screen as " + file);

		// focusWindow = App.focusedWindow()
		// regionImage = capture(focusWindow)
		// shutil.move(regionImage, os.path.join(r'C:\Screenshots', 'Dummy1.png'))

		InputStream inputStream = new FileInputStream("./logs/Login.jpg");
		// Get the contents of an InputStream as a byte[].
		byte[] bytes = IOUtils.toByteArray(inputStream);
		// Adds a picture to the workbook
		int pictureIdx = libroSalida.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
		// close the input stream
		inputStream.close();
		// Returns an object that handles instantiating concrete classes
		CreationHelper helper = libroSalida.getCreationHelper();
		// Creates the top-level drawing patriarch.
		Drawing drawing = hojaSalida.createDrawingPatriarch();
		// Create an anchor that is attached to the worksheet
		ClientAnchor anchor = helper.createClientAnchor();
		// create an anchor with upper left cell _and_ bottom right cell
		anchor.setCol1(1); // Column B
		// sheet.autoSizeColumn(0);
		anchor.setRow1(1); // Row 3
		anchor.setCol2(2); // Column C
		anchor.setRow2(2); // Row 4
		// Creates a picture
		Picture pict = drawing.createPicture(anchor, pictureIdx); // pict.resize(); //don't do that. Let the anchor
																	// resize the image!
		// Create the Cell B3
		Cell cell = hojaSalida.createRow(1).createCell(1);
		// set width to n character widths = count characters * 256
		int widthUnits = 100 * 256;
		hojaSalida.setColumnWidth(0, widthUnits);
		hojaSalida.setColumnWidth(1, widthUnits);
		// set height to n points in twips = n * 20
		short heightUnits = 100 * 60;
		cell.getRow().setHeight(heightUnits);

		Date fecha = new Date();
		Timestamp hora = new Timestamp(fecha.getTime());
		String hh = hora.toString();
		FileOutputStream fileOut = null;

// ===============================================================

		// FileInputStream obtains input bytes from the image file
		InputStream captura = new FileInputStream(nombreImagen);

		// Get the contents of an InputStream as a byte[].
		// byte[] bytes = IOUtils.toByteArray(captura);

		// Adds a picture to the workbook
		// int pictureIdx = libroSalida.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);

		// close the input stream
		captura.close();

		// Returns an object that handles instantiating concrete classes
		XSSFCreationHelper helperLibro = libroSalida.getCreationHelper();

		// Creates the top-level drawing patriarch.
		// Drawing<?> drawing = hojaSalida.createDrawingPatriarch();

		// Create an anchor that is attached to the worksheet
		ClientAnchor anchorWorksheet = helper.createClientAnchor();

		// create an anchor with upper left cell _and_ bottom right cell
		anchor.setCol1(2);
		anchor.setRow1(1);
		anchor.setCol2(3);
		anchor.setRow2(2);

		// Don't resize the picture. Let the anchor resize the image!
		filaSalida.setHeight((short) 5000);

		// open an OutputStream to save written into XLSX file
		FileOutputStream os = new FileOutputStream("./logs/logs.xlsx");
		libroSalida.write(os);

// ------------------------------- FIN DE INSERCION DE REGISTROS DE DATOS ---------------------------

		libroSalida.close();
	} // fin guardarImagen

	public static void log(String cadena) throws IOException {

		
		FileOutputStream outputStream = null;
		File archivo = null;
		int columna = 0;
		XSSFWorkbook libroSalida = new XSSFWorkbook();
		XSSFSheet hojaSalida = libroSalida.createSheet("Hoja1");
		XSSFRow filaSalida = null;
		XSSFCell celda = null;

//------------------ DEFINO ALGUNOS FORMATOS PARA UTILIZAR ----------------
		XSSFCellStyle alinearContenido = libroSalida.createCellStyle();
		Map<String, Object> formatearContenido = new HashMap<String, Object>();

		// Alinear el contenido de las celdas hacia arriba y al centro
		alinearContenido.setVerticalAlignment(VerticalAlignment.TOP);
		alinearContenido.setAlignment(HorizontalAlignment.CENTER);

		// Agregar bordes a las celdas
		formatearContenido.put(CellUtil.BORDER_TOP, BorderStyle.THIN);
		formatearContenido.put(CellUtil.BORDER_BOTTOM, BorderStyle.THIN);
		formatearContenido.put(CellUtil.BORDER_LEFT, BorderStyle.THIN);
		formatearContenido.put(CellUtil.BORDER_RIGHT, BorderStyle.THIN);
		formatearContenido.put(CellUtil.WRAP_TEXT, true);
		// Definir ancho de columna
		hojaSalida.setColumnWidth(columna, 6000);
		hojaSalida.setColumnWidth(columna + 1, 20000);
		hojaSalida.setColumnWidth(columna + 2, 25000);

// ----------------------- FIN DE DEFINICION DE FORMATOS -------------------

// -----------------VERIFICO SI EL ARCHIVO EXISTE -----------------------------------

		archivo = new File("./logs/logs.xlsx");
		String target = "./logs/backups/logs_" + new SimpleDateFormat("ddMMMMyyyy_HHmmss'.xlsx'").format(new Date());
		Path targetFilePath = Paths.get(target);
		Path sourceFilePath = Paths.get("./logs/logs.xlsx");

		if (archivo.exists() && !archivo.isDirectory()) { // Si el archivo SI existe entra por ac�...
//----------- El archivo existe. Hago el backup.. ----------------------------------------
			try {
				logger.info("Generando archivo de respaldo...");
				File theDir = new File("./logs/backups/"); // Defining Directory/Folder Name
				try {
					if (!theDir.exists()) { // Checks that Directory/Folder Doesn't Exists!
						boolean result = theDir.mkdir();
						if (result) {
							JOptionPane.showMessageDialog(null, "Se gener� backup!");
						}
					}
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, e);
				}

				Files.copy(sourceFilePath, targetFilePath);

			} catch (FileNotFoundException e) {
				// e.printStackTrace();
				System.out.println("Archivo inexistente.No se pudo hacer el backup. Continuamos...");
			}
//--------------------- Fin del backup.. ------------------------------------------

		} else if (!archivo.exists()) {

		}
//------------------- El archivo NO existe ----------------------------------------
		BufferedWriter escritor;
		escritor = new BufferedWriter(new FileWriter(archivo));
		escritor.write("");
		escritor.close();

// ------------------- ESCRIBO LOS TITULOS DE LAS COLUMNAS -----------------
		logger.info("Escribiendo columnas...");

		// Creo la fila y la enumero Fila 1
		filaSalida = (XSSFRow) hojaSalida.createRow(0);
		filaSalida.setRowNum(0);

		// Creo la celda A1
		celda = filaSalida.createCell(0);
		celda.setCellStyle(alinearContenido);
		CellUtil.setCellStyleProperties(celda, formatearContenido);
		String updateCell = celda.getStringCellValue();
		if (updateCell == null || updateCell.isEmpty()) {
			// Escribo el t�tulo de la columna A
			celda.setCellValue("Fecha y Hora");
		} else {
			System.out.println("Se intent� escribir en una celda que ten�a datos!!");
		}

		// Creo la celda B1
		celda = filaSalida.createCell(1);
		celda.setCellStyle(alinearContenido);
		CellUtil.setCellStyleProperties(celda, formatearContenido);
		// Escribo el t�tulo de la columna B
		celda.setCellValue("Informaci�n");

		// Creo la celda C1
		celda = filaSalida.createCell(2);
		celda.setCellStyle(alinearContenido);
		CellUtil.setCellStyleProperties(celda, formatearContenido);
		// Escribo el t�tulo de la columna C
		celda.setCellValue("Captura de pantalla");

		String EXCEL_DE_SALIDA = "./logs/logs.xlsx";
		// System.out.println("Nombre del Reporte: "+EXCEL_DE_SALIDA);

		outputStream = new FileOutputStream(EXCEL_DE_SALIDA);
		libroSalida.write(outputStream);
		outputStream.flush();

// -------------------- FIN DE LOS TITULOS DE LAS COLUMNAS ---------------------

		// Aqu� el archivo ya existe.

		// try {

		// FileInputStream archivoTemp = new FileInputStream(new File(EXCEL_DE_SALIDA));
		// Workbook libro = WorkbookFactory.create(inputStream);
		// wb = (XSSFWorkbook) WorkbookFactory.create(archivoTemp);

		// hojaSalida = wb.getSheetAt(0);
		// String nI=("./logs/").concat(nombreImagen);

// --------------------- INSERTO REGISTROS DE DATOS----------------------------------
		// get the last row number to append new
		// int rowCount = hojaSalida.getLastRowNum();

		// File myFile = new File("./logs/logs.xlsx");
		// FileInputStream fis = new FileInputStream(myFile);
		// libroSalida = new XSSFWorkbook(fis);

		// Alinear el contenido de las celdas hacia arriba y al centro
		alinearContenido.setVerticalAlignment(VerticalAlignment.TOP);
		alinearContenido.setAlignment(HorizontalAlignment.CENTER);

		// Agregar bordes a las celdas
		formatearContenido.put(CellUtil.BORDER_TOP, BorderStyle.THIN);
		formatearContenido.put(CellUtil.BORDER_BOTTOM, BorderStyle.THIN);
		formatearContenido.put(CellUtil.BORDER_LEFT, BorderStyle.THIN);
		formatearContenido.put(CellUtil.BORDER_RIGHT, BorderStyle.THIN);
		formatearContenido.put(CellUtil.WRAP_TEXT, true);

		hojaSalida = libroSalida.getSheet("Hoja1");

		// Creo la fila y la enumero Fila 2
		filaSalida = (XSSFRow) hojaSalida.createRow(1);
		filaSalida.setRowNum(1);

		// Creo la celda A2
		celda = filaSalida.createCell(0);
		// celda.setCellStyle(alinearContenido);
		// CellUtil.setCellStyleProperties(celda, formatearContenido);
		// Date fecha = new Date();
		// Timestamp hora= new Timestamp(fecha.getTime());
		// String hh=hora.toString();
		String fechaHora = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date());
		celda.setCellStyle(alinearContenido);
		CellUtil.setCellStyleProperties(celda, formatearContenido);
		celda.setCellValue(fechaHora);

		// Creo la celda B2
		celda = filaSalida.createCell(1);
		celda.setCellStyle(alinearContenido);
		CellUtil.setCellStyleProperties(celda, formatearContenido);
		celda.setCellValue(cadena);

		// Creo la celda C2
		celda = filaSalida.createCell(2);
		celda.setCellStyle(alinearContenido);
		CellUtil.setCellStyleProperties(celda, formatearContenido);
		celda.setCellValue("Captura de pantalla");

		/*
		 * //FileInputStream obtains input bytes from the image file InputStream
		 * captura= new FileInputStream(nI); InputStream captura= new
		 * FileInputStream("./images.sikuli/Login.jpg");
		 * 
		 * //Get the contents of an InputStream as a byte[]. byte[] bytes =
		 * IOUtils.toByteArray(captura);
		 * 
		 * //Adds a picture to the workbook int pictureIdx =
		 * libroSalida.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
		 * 
		 * close the input stream captura.close();
		 * 
		 * //Returns an object that handles instantiating concrete classes
		 * XSSFCreationHelper helper = libroSalida.getCreationHelper();
		 * 
		 * //Creates the top-level drawing patriarch. //Drawing<?> drawing =
		 * hojaSalida.createDrawingPatriarch();
		 * 
		 * //Create an anchor that is attached to the worksheet ClientAnchor anchor =
		 * helper.createClientAnchor();
		 * 
		 * //create an anchor with upper left cell _and_ bottom right cell
		 * anchor.setCol1(2); anchor.setRow1(1); anchor.setCol2(3); anchor.setRow2(2);
		 */
		// Don't resize the picture. Let the anchor resize the image!
		filaSalida.setHeight((short) 5000);

		// open an OutputStream to save written into XLSX file
		FileOutputStream os = new FileOutputStream("./logs/logs.xlsx");
		libroSalida.write(os);

// ------------------------------- FIN DE INSERCION DE REGISTROS DE DATOS ---------------------------
		try {
			libroSalida.close();
			outputStream.close();
		} catch (Exception e) {
		}

	} // fin guardarImagen

	public static void log(String cadena, String nombreImagen) throws IOException {
		FileOutputStream archivoExcelDeSalida = null;
		XSSFWorkbook libroSalida = null;
		Sheet hojaSalida = null;
		Cell celda = null;
		XSSFRow filaSalida = null;
		File archivo = null;
		int columna = 0;

		// byte[] imageBytesAS = null;
		// int pictureureASIdx = 0;
		// CreationHelper helper = null;
		// Drawing drawing = null;
		// ClientAnchor ancla = null;
		// Picture dibujoImagen = null;
		Map<String, Object> formatearContenido = null;

		libroSalida = new XSSFWorkbook();
		hojaSalida = libroSalida.createSheet("Hoja1");

//------------------ DEFINO ALGUNOS FORMATOS PARA UTILIZAR ----------------
		XSSFCellStyle alinearContenido = libroSalida.createCellStyle();
		alinearContenido.setVerticalAlignment(VerticalAlignment.TOP);
		alinearContenido.setAlignment(HorizontalAlignment.CENTER);

		formatearContenido = new HashMap<String, Object>();
		// border around a cell
		formatearContenido.put(CellUtil.BORDER_TOP, BorderStyle.THIN);
		formatearContenido.put(CellUtil.BORDER_BOTTOM, BorderStyle.THIN);
		formatearContenido.put(CellUtil.BORDER_LEFT, BorderStyle.THIN);
		formatearContenido.put(CellUtil.BORDER_RIGHT, BorderStyle.THIN);
		formatearContenido.put(CellUtil.WRAP_TEXT, true);

		// Defino ancho de columna
		hojaSalida.setColumnWidth(columna, 6000);
		hojaSalida.setColumnWidth(columna + 1, 20000);
		hojaSalida.setColumnWidth(columna + 2, 25000);

// ----------------------- FIN DE DEFINICION DE FORMATOS -------------------

// -----------------VERIFICO SI EL ARCHIVO EXISTE -----------------------------------
		archivo = new File("./logs/logs.xlsx");
		String target = "./logs/backups/logs_" + new SimpleDateFormat("ddMMMMyyyy_HHmmss'.xlsx'").format(new Date());
		Path targetFilePath = Paths.get(target);
		Path sourceFilePath = Paths.get("./logs/logs.xlsx");

		if (archivo.exists() && !archivo.isDirectory()) { // Si el archivo SI existe entra por ac�...

//----------- El archivo existe. Hago el backup.. ----------------------------------------
			try {
				File theDir = new File("./logs/backups/"); // Defining Directory/Folder Name
				try {
					if (!theDir.exists()) { // Checks that Directory/Folder Doesn't Exists!
						boolean result = theDir.mkdir();
						if (result) {
							JOptionPane.showMessageDialog(null, "Se gener� backup!");
						}
					}
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, e);
				}

				// Files.move(sourceFilePath, targetFilePath);
				Files.copy(sourceFilePath, targetFilePath);

			} catch (FileNotFoundException e) {
				// e.printStackTrace();
				System.out.println("Archivo inexistente.No se pudo hacer el backup. Continuamos...");
			}
//--------------------- Fin del backup.. ------------------------------------------

		} else if (!archivo.exists()) {
//------------------- El archivo NO existe ----------------------------------------

// ------------------- ESCRIBO LOS TITULOS DE LAS COLUMNAS -----------------
			// Creo la fila y la enumero Fila 1
			filaSalida = (XSSFRow) hojaSalida.createRow(0);
			filaSalida.setRowNum(0);

			// Creo la celda A1
			celda = filaSalida.createCell(0);
			celda.setCellStyle(alinearContenido);
			CellUtil.setCellStyleProperties(celda, formatearContenido);
			String updateCell = celda.getStringCellValue();
			if (updateCell == null || updateCell.isEmpty()) {
				// Me posiciono en la celda A1
				// hojaSalida.showInPane(fila, columna);
				// Escribo el t�tulo de la columna A
				celda.setCellValue("Fecha y Hora");
			} else {
				System.out.println("Se intent� escribir en una celda que ten�a datos!!");
			}

			// Creo la celda B1
			celda = filaSalida.createCell(1);
			celda.setCellStyle(alinearContenido);
			CellUtil.setCellStyleProperties(celda, formatearContenido);
			// Escribo el t�tulo de la columna B
			celda.setCellValue("Informaci�n");

			// Creo la celda C1
			celda = filaSalida.createCell(2);
			celda.setCellStyle(alinearContenido);
			CellUtil.setCellStyleProperties(celda, formatearContenido);
			// Escribo el t�tulo de la columna C
			celda.setCellValue("Captura de pantalla");

			archivoExcelDeSalida = new FileOutputStream("./logs/logs.xlsx");
			libroSalida.write(archivoExcelDeSalida);
			libroSalida.close();
			archivoExcelDeSalida.flush();
			archivoExcelDeSalida.close();

		} // fin del si el archivo existe

// -------------------- FIN DE LOS TITULOS DE LAS COLUMNAS ---------------------

		// Aqu� el archivo ya existe.

		// try {

		// FileInputStream archivoTemp = new FileInputStream(new File(EXCEL_DE_SALIDA));
		// Workbook libro = WorkbookFactory.create(inputStream);
		// wb = (XSSFWorkbook) WorkbookFactory.create(archivoTemp);

		// hojaSalida = wb.getSheetAt(0);
		// String nI=("./logs/").concat(nombreImagen);

// --------------------- INSERTO REGISTROS DE DATOS----------------------------------
		// get the last row number to append new
		// int rowCount = hojaSalida.getLastRowNum();

		File myFile = new File("./logs/logs.xlsx");
		FileInputStream fis = new FileInputStream(myFile);
		libroSalida = new XSSFWorkbook(fis);
		hojaSalida = libroSalida.getSheet("Hoja1");

		// Creo la fila y la enumero Fila 2
		filaSalida = (XSSFRow) hojaSalida.createRow(1);
		filaSalida.setRowNum(1);

		// ----------------------------------------INICIO FORMATO DE
		// CELDAS-----------------------------------------------
		alinearContenido.setVerticalAlignment(VerticalAlignment.TOP);
		alinearContenido.setAlignment(HorizontalAlignment.CENTER);

		formatearContenido = new HashMap<String, Object>();
		// border around a cell
		formatearContenido.put(CellUtil.BORDER_TOP, BorderStyle.THIN);
		formatearContenido.put(CellUtil.BORDER_BOTTOM, BorderStyle.THIN);
		formatearContenido.put(CellUtil.BORDER_LEFT, BorderStyle.THIN);
		formatearContenido.put(CellUtil.BORDER_RIGHT, BorderStyle.THIN);
		formatearContenido.put(CellUtil.WRAP_TEXT, true);

		// Defino ancho de columna
		hojaSalida.setColumnWidth(columna, 6000);
		hojaSalida.setColumnWidth(columna + 1, 20000);
		hojaSalida.setColumnWidth(columna + 2, 25000);
		// ----------------------------------------FIN FORMATO DE
		// CELDAS-----------------------------------------------
		// Creo la celda A2
		celda = filaSalida.createCell(0);
		// celda.setCellStyle(alinearContenido);
		// CellUtil.setCellStyleProperties(celda, formatearContenido);
		// Date fecha = new Date();
		// Timestamp hora= new Timestamp(fecha.getTime());
		// String hh=hora.toString();
		String fechaHora = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date());
		celda.setCellValue(fechaHora);

		// Creo la celda B2
		celda = filaSalida.createCell(1);
		// celda.setCellStyle(alinearContenido);
		// CellUtil.setCellStyleProperties(celda, formatearContenido);
		celda.setCellValue(cadena);

		// Creo la celda C2
		celda = filaSalida.createCell(2);
		// celda.setCellStyle(alinearContenido);
		// CellUtil.setCellStyleProperties(celda, formatearContenido);

		// FileInputStream obtains input bytes from the image file
		// String nI=("./logs/").concat(nombreImagen);
		// InputStream captura= new FileInputStream(nI);

		// Get the contents of an InputStream as a byte[].
		// byte[] bytes = IOUtils.toByteArray(captura);

		// Adds a picture to the workbook
		// int pictureIdx = libroSalida.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);

		// close the input stream
		// captura.close();

		// Returns an object that handles instantiating concrete classes
		XSSFCreationHelper helper = libroSalida.getCreationHelper();

		// Creates the top-level drawing patriarch.
		// Drawing<?> drawing = hojaSalida.createDrawingPatriarch();

		// Create an anchor that is attached to the worksheet
		ClientAnchor anchor = helper.createClientAnchor();

		// create an anchor with upper left cell _and_ bottom right cell
		anchor.setCol1(2);
		anchor.setRow1(1);
		anchor.setCol2(3);
		anchor.setRow2(2);

		// Don't resize the picture. Let the anchor resize the image!
		filaSalida.setHeight((short) 5000);

		// open an OutputStream to save written into XLSX file
		FileOutputStream os = new FileOutputStream("./logs/logs.xlsx");
		libroSalida.write(os);

// ------------------------------- FIN DE INSERCION DE REGISTROS DE DATOS ---------------------------

		libroSalida.close();
	} // fin guardarImagen

	public static void main(String[] args) throws Exception {
		// guardarImagen("Pantalla de Login", "./images.sikuli/Login/WLogin.jpg");
		new ExcelLogger();
		// PropertyConfigurator.configure("./resources/log4j.properties");
		// logger.info ("Iniciando automatizaci�n...");// preparo el logger...);
		java.util.Date fecha = new java.util.Date();
		Timestamp hora = new Timestamp(fecha.getTime());
		log("Test");
		// Logger.getLogger("Fecha: "+fecha.toString()+"Hora: "+hora.toString() );//
		// preparo el logger...

	}
}