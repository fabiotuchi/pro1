package utilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class Connetion {
	static WebDriver chrome_driver = null;
	static WebDriver iexplorer_driver = null;

			
	public static WebDriver openCDriver () {
		System.setProperty("webdriver.chrome.driver", "libs//chromedriver.exe");
		chrome_driver = new ChromeDriver();
		return chrome_driver;			
	}
	
	public static void closeCDriver () {
		chrome_driver.quit();
	}
	
	public static WebDriver openIDriver () {
		iexplorer_driver = new InternetExplorerDriver();
		return iexplorer_driver;			
	}
	
	public static void closeIDriver () {
		iexplorer_driver.quit();
	}
	
	public static void main(String args) {

	}
}
