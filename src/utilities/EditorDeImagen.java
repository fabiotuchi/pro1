package utilities;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.Buffer;
import java.util.Iterator;
import java.util.NoSuchElementException;

import javax.imageio.ImageIO;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Location;
import org.sikuli.script.Match;
import org.sikuli.script.Region;
import org.sikuli.script.Screen;

import sikulix.ManipularDatos;
import sikulix.OCR;
import utilities.EditorDeImagen;
import sikulix.ImageFrame;

public class EditorDeImagen {
	public static Screen screen1 = null;

	  /**
     * Resizes an image to a absolute width and height (the image may not be
     * proportional)
     * @param inputImagePath Path of the original image
     * @param outputImagePath Path to save the resized image
     * @param scaledWidth absolute width in pixels
     * @param scaledHeight absolute height in pixels
     * @throws IOException
     */
	    public static void resize(String inputImagePath,
	            String outputImagePath, int scaledWidth, int scaledHeight)
	            throws IOException {
	        // reads input image
	        File inputFile = new File(inputImagePath);
	        BufferedImage inputImage = ImageIO.read(inputFile);
	 
	        // creates output image
	        BufferedImage outputImage = new BufferedImage(scaledWidth,
	                scaledHeight, inputImage.getType());
	 
	        // scales the input image to the output image
	        Graphics2D g2d = outputImage.createGraphics();
	        g2d.drawImage(inputImage, 0, 0, scaledWidth, scaledHeight, null);
	        g2d.dispose();
	 
	        // extracts extension of output file
	        String formatName = outputImagePath.substring(outputImagePath
	                .lastIndexOf(".") + 1);
	 
	        // writes to output file
	        ImageIO.write(outputImage, formatName, new File(outputImagePath));
	    }
	 
	    /**
	     * Resizes an image by a percentage of original size (proportional).
	     * @param inputImagePath Path of the original image
	     * @param outputImagePath Path to save the resized image
	     * @param percent a double number specifies percentage of the output image
	     * over the input image.
	     * @throws IOException
	     */
	    public static void resize(String inputImagePath,
	            String outputImagePath, double percent) throws IOException {
	        File inputFile = new File(inputImagePath);
	        BufferedImage inputImage = ImageIO.read(inputFile);
	        int scaledWidth = (int) (inputImage.getWidth() * percent);
	        int scaledHeight = (int) (inputImage.getHeight() * percent);
	        resize(inputImagePath, outputImagePath, scaledWidth, scaledHeight);
	    }


	public void editimage(String nombreimagen, String nombreimagen2,String nombre) throws IOException, FindFailed {
	String inputImagePath = imgtemporaria(nombreimagen, nombreimagen2);
	String outputImagePath = "C:\\xcfotech\\numeros\\"+nombre+".png";

    try { double percent = 3;
        resize(inputImagePath, outputImagePath, percent);
    } catch (IOException ex) {System.out.println("Error resizing the image.");ex.printStackTrace();}
	  try {	Thread.sleep(5000);} catch (InterruptedException e) {e.printStackTrace();}
	}
	
	
	public void editimageOC(String nombreimagen, String nombreimagen2,String nombre) throws IOException, FindFailed {
	String inputImagePath = imgtemporariaOC(nombreimagen, nombreimagen2, 0);
	String outputImagePath = "C:\\xcfotech\\numeros\\"+nombre+".png";
    try { double percent = 3;
        resize(inputImagePath, outputImagePath, percent);
    } catch (IOException ex) {System.out.println("Error resizing the image.");ex.printStackTrace();}
	  try {	Thread.sleep(5000);} catch (InterruptedException e) {e.printStackTrace();}
	}
	
	
	public void editimage2(String nombreimagen, String nombreimagen2,String nombre) throws IOException, FindFailed {
	String inputImagePath = imgtemporaria2(nombreimagen, nombreimagen2);
	String outputImagePath = "C:\\xcfotech\\numeros\\"+nombre+".png";
    try { double percent = 3;
        resize(inputImagePath, outputImagePath, percent);
    } catch (IOException ex) {System.out.println("Error resizing the image.");ex.printStackTrace();}
	  try {	Thread.sleep(5000);} catch (InterruptedException e) {e.printStackTrace();}
	}
	
	// Lo que viene estaba en la clase BP
	public static String dameDato(String d) {
		ManipularDatos.setArchivoExcel(ManipularDatos.getArchivoExcel());
		return ManipularDatos.obtenerDatoDeCelda(ManipularDatos.getArchivoExcel(), d);
	}
	
	public static OCR ocr = null;

	public void SelectRegionyClick(String imagenregion, String imagenclick) throws FindFailed {
		espera();
		screen1 = new Screen();
		Match f = screen1.find(img(imagenregion)).find(img(imagenclick));
		f.click();
	}

	public void SelectRegionyClickAll(String imagenregion, String imagenclick) throws FindFailed {
		Match f = screen1.find(img(imagenregion));
		Iterator<Match> r = f.findAll(img(imagenclick));
		r.next().click();
	}

	public static void SelectRegionyDClick(String imagenregion, String imagenclick) throws FindFailed {
		espera();
		Match f = screen1.find(img(imagenregion));
		espera();
		Region r = f.find(img(imagenclick));
		espera();
		r.doubleClick();
	}

	public static String img(String nombreimagen) {
		String repositorio = "./images.sikuli/";
		String imagen = repositorio + nombreimagen + ".png";
		return imagen;
	}

	public static void Login(String perfil) throws InterruptedException, FindFailed {
		clickandtype("UsuarioLogin", dameDato("A3"));
		clickandtype("PswdLogin", dameDato("B3"));
		click("PerfilLogin");
		if (perfil.contains(dameDato("C3"))) {
			click("PerfilOPerez");
		} else {
			click("OrganoRector");
		}
		click("AceptarLogin");
	}

	public static void deslogin() throws FindFailed {
		click("desconectar");
	}

	///// acciones/////
	public void wait(String nombreimagen) throws FindFailed {
		screen1.wait(img(nombreimagen), 10);
	}

	public static void click(String nombreimagen) throws FindFailed {
		espera();
		screen1=new Screen();
		screen1.wait(img(nombreimagen), 15);
		screen1.click(img(nombreimagen));
		espera();
	}

	public void find(String nombreimagen) throws FindFailed {
		screen1.wait(img(nombreimagen), 15);
		screen1.find(img(nombreimagen));
	}

	public static void dclick(String nombreimagen) throws FindFailed {
		
		screen1.wait(img(nombreimagen), 15);
		screen1.doubleClick(img(nombreimagen));
	}

	public void type(String txt) {
		espera();
		
		screen1.type(txt);
	}

	public static void clickandtype(String nombreimagen, String txt) throws FindFailed {
		try {
		espera();
		screen1 = new Screen();
		screen1.type(img(nombreimagen), txt);}
		catch(Exception e) {
			System.out.println("Oh oh! fall� el clickandtype!");
		}
	}

	public void dclickfocustype(String nombreimagen, int x, String txt) throws FindFailed {
		
		Match imagen = screen1.find(img(nombreimagen));
		imagen.setTargetOffset(x, 0);
		imagen.doubleClick();
		espera();
		type(txt);
	}

	public void clickfocustype(String nombreimagen, int x, String txt) throws FindFailed {
		
		Match imagen = screen1.find(img(nombreimagen));
		imagen.setTargetOffset(x, 0);
		espera();
		imagen.click();
		espera();
		type(txt);
		espera();

	}

	public void clickfocustype(String nombreimagen, int x, int y, String txt) throws FindFailed {
		espera();
		
		Match imagen = screen1.find(img(nombreimagen));
		imagen.setTargetOffset(x, y);
		espera();
		imagen.click();
		espera();
		type(txt);
	}

	public void dclickfocustype(String nombreimagen, int x, int y, String txt) throws FindFailed {
		espera();
		
		Match imagen = screen1.find(img(nombreimagen));
		imagen.setTargetOffset(x, y);
		espera();
		imagen.doubleClick();
		espera();
		type(txt);
	}

	public void clickfocus(String nombreimagen, int x) throws FindFailed {
		
		Match imagen = screen1.find(img(nombreimagen));
		imagen.setTargetOffset(x, 0);
		imagen.click();
		espera();
	}

	public void clickfocus(String nombreimagen, int x, int y) throws FindFailed {
		
		Match imagen = screen1.find(img(nombreimagen));
		imagen.setTargetOffset(x, y);
		espera();
		imagen.click();
		espera();
	}

	public void dclickfocus(String nombreimagen, int x, int y) throws FindFailed {
		
		Match imagen = screen1.find(img(nombreimagen));
		imagen.setTargetOffset(x, y);
		imagen.doubleClick();
	}

	public static void espera() {
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void espera2() {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	///// Ingresos/////
	public static void IngresarSolicitudDeGasto() throws InterruptedException, FindFailed {
		boolean verifico = verificarimagen("Compras");
		System.out.println(verifico);
		if (verifico = true) {
			click("Compras");
			dclick("SolicitudDeGasto");
			dclick("ingreso");
			click("IrAIngreso");
		} else {
			click("sigaf");
			click("Compras");
			dclick("ingreso");
		}
	}

	public static void IngresarParametroDeLaContratacion() throws InterruptedException, FindFailed {
		dclick("ParametrosDeLaContratacion");
		SelectRegionyDClick("Parametros", "ingreso");
		click("IrAIngreso");

	}

	public static void IngresarAdjudicacion() throws FindFailed {
		dclick("Adjudicacion");
		espera();
		SelectRegionyDClick("AdjudicacionOp", "IngresoDeOriginales");
		click("IrAIngreso");
	}

	public static void IngresarAutorizacionAutoridadCompetente() throws FindFailed {
		dclick("AutorizarAutoridadCompetente");
		espera();
		SelectRegionyDClick("RegAutoprizarAutoridad", "AutorizarActoDeAdjudicacion");
	}

	public static void IngresarOrdenDeCompraYContrato() throws FindFailed {
		dclick("OrdenDeCompraYContrato");
		espera();
		SelectRegionyDClick("RegOrdenDeCompraYContrato", "ingreso");
		click("IrAIngreso");

	}

	public void IngresarPendienteDeEnvio() throws FindFailed {
		espera();
		SelectRegionyDClick("PendienteDeEnvio", "PendienteDeEnvio");
	}
	// ***************************************************************************************************+++

	public static void volvermenu() throws FindFailed {
		dclick("volvermenu");
	}

	@SuppressWarnings("unused")
	public void SacarFoto(String nombreimagen, String nombreimagen2, String nombre) throws FindFailed, IOException {
		EditorDeImagen EDI = new EditorDeImagen();
		String outputImagePath = "C:\\Users\\ljerez\\Desktop\\numeros\\" + nombre + ".png";
		EDI.editimage2(nombreimagen, nombreimagen2, nombre);
		ImageFrame f = new ImageFrame(outputImagePath);
	}

	public static String GetTextFromImg(String nombreimagen, String nombreimagen2, String nombre)
			throws FindFailed, IOException {
		OCR.setStoragePath("./resources/glyphs");
		ocr = OCR.getSpec("numbers");
		EditorDeImagen EDI = new EditorDeImagen();
		String outputImagePath = "C:\\Users\\ljerez\\Desktop\\numeros\\" + nombre + ".png";
		EDI.editimage(nombreimagen, nombreimagen2, nombre);
		espera();
		ImageFrame f = new ImageFrame(outputImagePath);
		String text = ocr.read(f.getBounds());
		espera();
		f.close();
		return text;
	}

	public String GetTextFromImgOC(String nombreimagen, String nombreimagen2, String nombre)
			throws FindFailed, IOException {
		OCR.setStoragePath("./resources/glyphs");
		ocr = OCR.getSpec("numbers");
		EditorDeImagen EDI = new EditorDeImagen();
		String outputImagePath = "C:\\Users\\ljerez\\Desktop\\numeros\\" + nombre + ".png";
		EDI.editimageOC(nombreimagen, nombreimagen2, nombre);
		espera();
		ImageFrame f = new ImageFrame(outputImagePath);
		String text = ocr.read(f.getBounds());
		espera();
		f.close();
		return text;
	}

	public String GetTextFromImg2(String nombreimagen, String nombreimagen2, String nombre)
			throws FindFailed, IOException {
		OCR.setStoragePath("./resources/glyphs");
		ocr = OCR.getSpec("numbers");
		EditorDeImagen EDI = new EditorDeImagen();
		String outputImagePath = "C:\\Users\\ljerez\\Desktop\\numeros\\" + nombre + ".png";
		EDI.editimage2(nombreimagen, nombreimagen2, nombre);
		espera();
		ImageFrame f = new ImageFrame(outputImagePath);
		String text = ocr.read(f.getBounds());
		espera();
		f.close();
		return text;
	}

	public String imgtemporaria(String nombreimagen, String nombreimagen2) throws FindFailed {
		Match f = screen1.find(img(nombreimagen)).find(img(nombreimagen2));
		Location b = f.getTopRight();
		@SuppressWarnings("deprecation")
		String txt = f.moveTo(b).saveScreenCapture();
		return txt;
	}

	public String imgtemporariaOC(String nombreimagen, String nombreimagen2, int c) throws FindFailed { 
		Match f = screen1.find(img(nombreimagen)).find(img(nombreimagen2));
		Location b = f.getBottomLeft();
		@SuppressWarnings("deprecation")

		String txt = f.moveTo(b).saveScreenCapture();
		return txt;
	}

	public String imgtemporaria2(String nombreimagen, String nombreimagen2) throws FindFailed {

		Match f = screen1.find(img(nombreimagen)).find(img(nombreimagen2));
		Location b = f.getBottomLeft();
		@SuppressWarnings("deprecation")
		String txt = f.moveTo(b).saveScreenCapture();
		return txt;
	}

	public static boolean verificarimagen(String nombreimagen) {
		boolean a = true;
		try { 
			boolean b = screen1.find(img(nombreimagen)).isValid();
			System.out.println(b);
		} catch (NoSuchElementException | FindFailed e) {
			a = false;
		}
		return a;
	}

}
