package main;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import sikulix.ManipularDatos;
import utilities.OsCheck;

public class Login implements Runnable {
	static WebDriver driver = null;
	private static boolean doStop = false;
	String ELAB2 = "https://crs-test2.sabrehospitality.com/CC/login.aspx?ReturnUrl=%2fCC%2fdefault.aspx";

	public static synchronized void doStop() {
		driver.quit();
		doStop = true;
	}

	private synchronized boolean keepRunning() {
		return this.doStop == false;
	}

	@Override
	public void run() {

		System.out.println("Login started!");
		
		WebElement nombre = null, clave = null, signin_bt, icono, salir, skip_validation_bt, current_user, logout;

		OsCheck.OSType ostype=OsCheck.getOperatingSystemType();
		switch (ostype) {
		    case Windows: {
		    	System.out.println("Windows OS detected...");
				System.setProperty("webdriver.chrome.driver", "libs//chromedriver.exe");
		    	break;
		    }
		    case MacOS: break;
		    case Linux: {
		    	System.out.println("Linux OS detected...");
				System.setProperty("webdriver.chrome.driver", "libs//chromedriver");
		    	break;
		    }
		    case Other: break;
		}
		
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get(ELAB2);
		Assert.assertEquals("Control Center powered by the SynXis CRS", driver.getTitle()); // Validate that expected

		nombre = driver.findElement(By.id("LoginCntrl_UsernameTextBox")); // find username textbox
		nombre.click(); // click on it to have focus
		String user= ManipularDatos.obtenerDatoDeCelda("./resources/data.xlsx", "A2");
		nombre.sendKeys(user);// insert username
		clave = driver.findElement(By.id("LoginCntrl_PasswordTextBox"));// find password textbox
		clave.click();// click on it to have focus
		String pass=ManipularDatos.obtenerDatoDeCelda("./resources/data.xlsx", "B2");
		clave.sendKeys(pass);// insert password

		signin_bt = driver.findElement(By.name("LoginCntrl$LoginButton"));
		signin_bt.click();

		try {Thread.sleep(1000);} catch (InterruptedException e1) {e1.printStackTrace();}

		// Check if validation password appears..
		try {
			skip_validation_bt = driver.findElement(By.name("LoginCntrl$btnSkipValidation"));
			skip_validation_bt.click();
		} catch (Exception e) {
			System.out.println("Validation page didn't show up...");
		}

		try {Thread.sleep(1000);} catch (InterruptedException e1) {e1.printStackTrace();}


		try {
			current_user = driver.findElement(By.name("UserActionsHead"));
			current_user.click();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		try {
			logout = driver.findElement(By.xpath("//*[@id=\"UserActions\"]/ul/li[1]/ul/li[2]/a"));
			logout.click();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		driver.quit();

	}

}
