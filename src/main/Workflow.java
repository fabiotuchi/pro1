package main;

import java.io.IOException;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.ie.InternetExplorerDriver;

import sikulix.ManipularDatos;
import utilities.ExcelLogger;

public class Workflow {

	public static void main(String[] args) throws InterruptedException, IOException {
		WebElement nombre = null, clave = null, signin_bt, icono, salir, skip_validation_bt, current_user, logout;
		ManipularDatos excel = new ManipularDatos();
		String archivoDatos = ".//resources//data.xlsx";
		
		System.setProperty("webdriver.chrome.driver", ".//libs//chromedriver.exe");
		// WebDriver driver = new InternetExplorerDriver();
		WebDriver driver = new ChromeDriver();
		
		// ------------------------------------------------------------------------------------------------------------------------
		driver.get("https://crs-test2.sabrehospitality.com/CC/login.aspx?ReturnUrl=%2fCC%2fdefault.aspx"); // Open
		driver.manage().window().maximize();																									// webpage
		Assert.assertEquals("Control Center powered by the SynXis CRS", driver.getTitle()); // Validate that expected
																							// webpage is Primary

		ExcelLogger.log("Iniciando con la pantalla de Login");
		ExcelLogger.log("Pantalla de Login", "Login");
		
		nombre = driver.findElement(By.id("LoginCntrl_UsernameTextBox")); // find username textbox
		nombre.click(); // click on it to have focus
		
		
		nombre.sendKeys(excel.obtenerDatoDeCelda(archivoDatos, "A2"));// insert username
		clave = driver.findElement(By.id("LoginCntrl_PasswordTextBox"));// find password textbox
		clave.click();// click on it to have focus
		clave.sendKeys(excel.obtenerDatoDeCelda(archivoDatos, "B2"));// insert password

		signin_bt = driver.findElement(By.name("LoginCntrl$LoginButton"));
		signin_bt.click();
		
		

/*
		Thread.sleep(1000);

		// Ver si la página que se muestra es la de validacion de mail
		try {
			skip_validation_bt = driver.findElement(By.name("LoginCntrl$btnSkipValidation"));
			skip_validation_bt.click();
		} catch (Exception e) {
			System.out.println("No apareci� la pantalla de validaci�n de email...");
		}

		Thread.sleep(1000);

		try {
			current_user = driver.findElement(By.name("UserActionsHead"));
			current_user.click();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		try {
			logout = driver.findElement(By.xpath("//*[@id=\"UserActions\"]/ul/li[1]/ul/li[2]/a"));
			logout.click();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
*/
		/*
		 * //Logout icono =
		 * driver.findElement(By.xpath("//*[@id=\"nb-collapse\"]/ul[2]/li[2]/a/span[2]")
		 * ); icono.click();
		 * 
		 * salir =
		 * driver.findElement(By.xpath("//*[@id=\"nb-collapse\"]/ul[2]/li[2]/ul/li[5]/a"
		 * )); salir.click();
		 * 
		 * //Validar proceso de autenticación
		 * 
		 * //Attempt to login without entering credentials signin_bt =
		 * driver.findElement(By.xpath("/html/body/div/div/div/form/div[4]/div/button"))
		 * ; signin_bt.click();
		 * 
		 * Thread.sleep(1000);
		 * 
		 * //Attempt to login givin only username nombre =
		 * driver.findElement(By.id("username")); //find username textbox
		 * nombre.click(); //click on it to have focus nombre.sendKeys("admin");//insert
		 * username signin_bt =
		 * driver.findElement(By.xpath("/html/body/div/div/div/form/div[4]/div/button"))
		 * ; signin_bt.click();
		 */


		// ------------------------------------------------------------------------------------------------------------------------
		
		driver.quit();

	}

}
