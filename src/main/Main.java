package main;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.logging.Logger;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import com.sun.corba.se.impl.encoding.CodeSetConversion.BTCConverter;

import utilities.TimerTime;
import utilities.UpdateDisplay;

public class Main extends JPanel {
	private static final long serialVersionUID = 1L;
	private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	private static ImageIcon icon = null;
	private static String monitorLog ="";
	private static JTextArea display = new JTextArea(monitorLog,80,25);
	
	/**
	 * @return the monitorLog
	 */
	public static String getMonitorLog() {
		return monitorLog;
	}

	/**
	 * @param monitorLog the monitorLog to set
	 */
	public static void setMonitorLog(String monitorLog) {
		Main.monitorLog = monitorLog;
	}

	Thread loginThread = null;

	private final Thread getLoginThread() {
		return loginThread;
	}

	private void setLoginThread(Thread t) {
		loginThread = t;
	}

	private static final String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm:ss";
	private static JFrame frame = new JFrame("Monitor");
	private static JTabbedPane tabbedPane = new JTabbedPane();
	private static GridLayout gridLayout1 = new GridLayout();
	private static 	JButton btQuit = new JButton("Salir");
	private static UpdateDisplay ud = new UpdateDisplay();
	private static Thread tud = new Thread(ud);
	private static JLabel tl = new JLabel();
	
	private void createTab1() {
		
		JComponent panel1 = new JPanel();
		icon = new ImageIcon(".//images//employee.gif");

		tabbedPane.addTab("Tab 1", icon, panel1, "Does nothing");
		tabbedPane.setMnemonicAt(0, KeyEvent.VK_1);

		JButton btIniciar = new JButton("Start");
		JButton btPausar = new JButton("Pausar");
		JButton btContinuar = new JButton("Continuar");
		JButton btInterrumpir = new JButton("Interrumpir");

		btIniciar.setBounds(100, 100, 140, 140);

		panel1.add(btIniciar);
		btIniciar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btIniciar.setEnabled(false);
				btPausar.setEnabled(true);
				btContinuar.setEnabled(false);
				btInterrumpir.setEnabled(true);
				Login loginPage = new Login();
				setLoginThread(new Thread(loginPage));
				loginThread.start();
				display.append(tl.getText() + ": Automation started. \n");
			}
		});

		btPausar.setBounds(100, 100, 140, 140);
		panel1.add(btPausar);
		btPausar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btPausar.setEnabled(false);
				btContinuar.setEnabled(true);
				loginThread.suspend();
				display.append(tl.getText() + ": Automation paused. \n");

			}
		});

		btContinuar.setBounds(100, 100, 140, 140);
		panel1.add(btContinuar);
		btContinuar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btPausar.setEnabled(true);
				btContinuar.setEnabled(false);
				loginThread.resume();
				display.append(tl.getText() + ": Automation resumed. \n");

			}
		});

		btInterrumpir.setBounds(100, 100, 140, 140);
		panel1.add(btInterrumpir);
		btInterrumpir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					if (!btIniciar.isEnabled()) {
						btIniciar.setEnabled(true);
						btPausar.setEnabled(false);
						btContinuar.setEnabled(false);
						btInterrumpir.setEnabled(false);
						Login.driver.close();
						display.append(tl.getText() + ": Automation interrumpted. \n");

						}
				} catch (NullPointerException ne) {
					// System.out.println("Error closing the webdriver.");
				} catch(Exception ee) {
					System.exit(0);
				}

				try {
					if (btIniciar.isEnabled())
						Login.doStop();
				} catch (Exception ex) {
					// System.out.println("Error stopping Login page.");
				}

				try {
					if (btIniciar.isEnabled())
						loginThread.interrupt();
				} catch (Exception e1) {
					// System.out.println("Error interrumping Login thread.");

				}
				btIniciar.setEnabled(true);

			}
		});

	}

	private void createTab2() {
		JComponent panel1 = new JPanel();
		icon = new ImageIcon(".//images//employee.gif");

		tabbedPane.addTab("Tab 2", icon, panel1, "Does nothing");
		tabbedPane.setMnemonicAt(0, KeyEvent.VK_1);

		JButton btSalir = new JButton("Salir");
		btSalir.setBounds(100, 100, 140, 140);

		panel1.add(btSalir);
		btSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				try {
					Login.driver.close();
				} catch (NullPointerException ne) {
					// System.out.println("Error closing the webdriver.");
				}
				try {
					Login.doStop();
				} catch (Exception ex) {
					// System.out.println("Error stopping Login page.");
				}

				try {
					// loginThread.interrupt();
				} catch (Exception e1) {
					// System.out.println("Error interrumping Login thread.");

				}
				// btIniciar.setEnabled(true);
				System.exit(0);
			}
		});

	}

	private void createTab3() {
		JComponent panel3 = new JLabel("Panel #3");
		icon = new ImageIcon(".//images//employee.gif");

		tabbedPane.addTab("Tab 3", icon, panel3, "Still does nothing");
		tabbedPane.setMnemonicAt(1, KeyEvent.VK_3);
		tabbedPane.setLocation(0, 700);

		JButton btSalir = new JButton("Salir");
		btSalir.setBounds(100, 100, 140, 140);

		panel3.add(btSalir);
		btSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				try {
					Login.driver.close();
				} catch (NullPointerException ne) {
					// System.out.println("Error closing the webdriver.");
				}
				try {
					Login.doStop();
				} catch (Exception ex) {
					// System.out.println("Error stopping Login page.");
				}

				try {
					// loginThread.interrupt();
				} catch (Exception e1) {
					// System.out.println("Error interrumping Login thread.");

				}
				// btIniciar.setEnabled(true);
				System.exit(0);
			}
		});

	}

	public Main() {
		LOGGER.info("Displaying Splash screen...");
		new Splash();

		gridLayout1.setRows(2);
		gridLayout1.setHgap(1);
		gridLayout1.setColumns(1);
		gridLayout1.setVgap(1);
		tabbedPane.setLayout(gridLayout1);

		tabbedPane.setLocation(0, 700);

		createTab1();
		createTab2();
		createTab3();

		// Add the tabbed pane to this panel.
		this.add(tabbedPane);

		// The following line enables to use scrolling tabs.
		tabbedPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);

		JScrollPane scroll = new JScrollPane(display);
		scroll.setPreferredSize(new Dimension(500,80));
		
		ud.run();
		this.add(scroll);
		
		TimerTime t = new TimerTime();
		tl = t.getTimeLabel();
		this.add(t);

		btQuit.setBounds(100, 100, 140, 140);
		this.add(btQuit);
		btQuit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				try {
					Login.driver.close();
				} catch (NullPointerException ne) {
					// System.out.println("Error closing the webdriver.");
				} catch(Exception ee) {
					System.exit(0);
				}
				try {
					Login.doStop();
				} catch (Exception ex) {
					// System.out.println("Error stopping Login page.");
					System.exit(0);
				}

				// btIniciar.setEnabled(true);
				display.append(tl.getText() + ": Hasta la vista...baby. \n");

				System.exit(0);
			}
		});

	}

	/**
	 * Create the GUI and show it. For thread safety, this method should be invoked
	 * from the event dispatch thread.
	 */
	private static void createAndShowGUI() {
		// Create and set up the window.
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new GridLayout());
		/*
		gridLayout1 = new GridLayout(); gridLayout1.setRows(1);
		gridLayout1.setHgap(1); gridLayout1.setColumns(1); gridLayout1.setVgap(1);
		frame.setLayout(gridLayout1);
		 */

		Main content = new Main();
		content.setVisible(true);

		frame.add(content);

		frame.pack();
		frame.setVisible(true);
		frame.setAlwaysOnTop(true);
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice defaultScreen = ge.getDefaultScreenDevice();
		Rectangle rect = defaultScreen.getDefaultConfiguration().getBounds();
		int x = (int) rect.getMaxX() - frame.getWidth();
		int y = (int) rect.getMaxY() - frame.getHeight();
		frame.setLocation(x, y);
	}

	public static void main(String[] args) {

		// Schedule a job for the event dispatch thread:
		// creating and showing this application's GUI.
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				// Turn off metal's use of bold fonts
				UIManager.put("swing.boldMetal", Boolean.FALSE);
				createAndShowGUI();

			}
		});
	}
}
