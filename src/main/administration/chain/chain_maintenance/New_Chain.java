package main.administration.chain.chain_maintenance;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import utilities.Connetion;
import utilities.ExcelLogger;

public class New_Chain extends ExcelLogger {

	public static void main(String[] args) throws InterruptedException, IOException {
		WebElement nombre = null, clave = null, signin_bt, icono, salir, skip_validation_bt, current_user, logout;
		WebDriver cdriver, idriver;
		
		cdriver = Connetion.openCDriver();
		
		cdriver.get("https://crs-test2.sabrehospitality.com/CC/login.aspx?ReturnUrl=%2fCC%2fdefault.aspx"); // Open

																											// webpage
		Assert.assertEquals("Control Center powered by the SynXis CRS", cdriver.getTitle()); // Validate that expected
																							// webpage is Primary

		ExcelLogger.log("P�gina de Login");

		nombre = cdriver.findElement(By.id("LoginCntrl_UsernameTextBox")); // find username textbox
		nombre.click(); // click on it to have focus
		nombre.sendKeys("fc_002");// insert username
		clave = cdriver.findElement(By.id("LoginCntrl_PasswordTextBox"));// find password textbox
		clave.click();// click on it to have focus
		clave.sendKeys("Pato2018!");// insert password

		signin_bt = cdriver.findElement(By.name("LoginCntrl$LoginButton"));
		signin_bt.click();

		Thread.sleep(1000);

		// Ver si la p�gina que se muestra es la de validaci�n de mail
		try {
			skip_validation_bt = cdriver.findElement(By.name("LoginCntrl$btnSkipValidation"));
			skip_validation_bt.click();
		} catch (Exception e) {
			System.out.println("No apareci� la pantalla de validaci�n de email...");
		}

		Thread.sleep(1000);

		try {
			current_user = cdriver.findElement(By.xpath("//*[@id=\"MenuRow\"]/div/ul[1]/li[5]/a"));
			current_user.click();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		try {
			logout = cdriver.findElement(By.xpath("//*[@id=\"MenuRow\"]/div/ul[1]/li[5]/div/ul/li[1]/a"));
			logout.click();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		try {
			logout = cdriver.findElement(By.cssSelector("#MenuRow > div > ul:nth-child(1) > li.active > div > div.thrdLvl > div > ul > li:nth-child(1) > a"));
			
			logout.click();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		
		try {
		ExcelLogger.guardarImagen("Administration > Chain > Chain Maintenance", "chain_maintenance");
		} catch (FileNotFoundException fnf) {
			fnf.printStackTrace();
		}
		/*
		 * //Logout icono =
		 * driver.findElement(By.xpath("//*[@id=\"nb-collapse\"]/ul[2]/li[2]/a/span[2]")
		 * ); icono.click();
		 * 
		 * salir =
		 * driver.findElement(By.xpath("//*[@id=\"nb-collapse\"]/ul[2]/li[2]/ul/li[5]/a"
		 * )); salir.click();
		 * 
		 * //Validar proceso de autenticación
		 * 
		 * //Attempt to login without entering credentials signin_bt =
		 * driver.findElement(By.xpath("/html/body/div/div/div/form/div[4]/div/button"))
		 * ; signin_bt.click();
		 * 
		 * Thread.sleep(1000);
		 * 
		 * //Attempt to login givin only username nombre =
		 * driver.findElement(By.id("username")); //find username textbox
		 * nombre.click(); //click on it to have focus nombre.sendKeys("admin");//insert
		 * username signin_bt =
		 * driver.findElement(By.xpath("/html/body/div/div/div/form/div[4]/div/button"))
		 * ; signin_bt.click();
		 */

		cdriver.quit();
		Connetion.closeCDriver();

	}

}
