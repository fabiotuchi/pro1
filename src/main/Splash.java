package main;

import java.awt.Color;
import java.awt.*;
import java.awt.Graphics;
import java.awt.MediaTracker;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JWindow;
import javax.swing.SwingUtilities;

/** This class display the Splash screen. */
public class Splash extends JWindow {

	public Splash()
	    {
	        super();
			
	        String filename=".//images//logo.jpg";
	        Frame f= new Frame(); 
	        int waitTime=3500;
	        JLabel l = new JLabel(new ImageIcon(filename));
	        getContentPane().add(l, BorderLayout.CENTER);
	        pack();
	        Dimension screenSize =
	          Toolkit.getDefaultToolkit().getScreenSize();
	        Dimension labelSize = l.getPreferredSize();
	        setLocation(screenSize.width/2 - (labelSize.width/2),
	                    screenSize.height/2 - (labelSize.height/2));
	        
	        addMouseListener(new MouseAdapter()
	            {
	                public void mousePressed(MouseEvent e)
	                {
	                    setVisible(false);
	                    dispose();
	                }
	            });
	        
	        final int pause = waitTime;
	        final Runnable closerRunner = new Runnable()
	            {
	                public void run()
	                {
	                    setVisible(false);
	                    Main.setMonitorLog("Chau Splash");
	        			dispose();
	                }
	            };
	            
	        Runnable waitRunner = new Runnable()
	            {
	                public void run()
	                {
	                    try
	                        {
	                            Thread.sleep(pause);
	                            SwingUtilities.invokeAndWait(closerRunner);
	                        }
	                    catch(Exception e)
	                        {
	                            //e.printStackTrace();
	                            // can catch InvocationTargetException
	                            // can catch InterruptedException
	                        }
	                }
	            };
	            
	        setVisible(true);
	        Thread splashThread = new Thread(waitRunner, "SplashThread");
	        splashThread.start();
	    }

}
